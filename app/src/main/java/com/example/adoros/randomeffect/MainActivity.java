package com.example.adoros.randomeffect;

import android.annotation.TargetApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private ImageButton button;

    @Override
    @TargetApi(21)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        button = findViewById(R.id.imageButton);
        final List<String> conditionList = new ArrayList<>();
        conditionList.add("Ты не выговариваешь букву \"Р\" 15 минут");
        conditionList.add("Ты не выговариваешь букву \"Л\" 15 минут");
        conditionList.add("Ты не выговариваешь букву \"К\" 15 минут");
        conditionList.add("Ты не выговариваешь букву \"А\" 15 минут");
        conditionList.add("Ты не выговариваешь букву \"Е\" 15 минут");
        conditionList.add("Ты не выговариваешь гласные 10 минут");
        conditionList.add("Ты не выговариваешь согласные 10 минут");
        conditionList.add("Ты не выговариваешь первые буквы слов 10 минут");
        conditionList.add("Ты не выговариваешь последние буквы слов 10 минут");
        conditionList.add("Ты не выговариваешь середениу слов 10 минут");
        conditionList.add("Ты все слова начинаешь букувой \"Б\" в течение 10 минут");
        conditionList.add("Ты все слова начинаешь букувой \"П\" в течение 10 минут");
        conditionList.add("Ты в основном мяукаешь вместо речи 15 минут");
        conditionList.add("Ты в основном чирикаешь вместо речи 15 минут");
        conditionList.add("Ты в основном жужжишь вместо речи 15 минут");
        conditionList.add("Весь твой словарный запас \"Чики-брики и в дамки\" в течение 10 минут");
        conditionList.add("Весь твой словарный запас \"Бляя, маслину поймал\" в течение 10 минут");
        conditionList.add("Очень хочется приседать, спонтанно приседай в течение 15 минут около раза в 30 сек.");
        conditionList.add("Очень хочется прыгать, спонтанно подпрыгивай в течение 15 минут около раза в 30 сек.");
        conditionList.add("Очень хочется отжиматься, спонтанно отжимайся в течение 15 минут около раза в 30 сек.");
        conditionList.add("Очень хочется танцевать, спонтанно танцуй в течение 15 минут около раза в 30 сек.");
        conditionList.add("Очень хочется петь, ты поешь все что хочешь сказать в течение 15 минут");
        conditionList.add("Ты в основном гавкаешь вместо речи 15 минут");
        conditionList.add("Ты в основном каркаешь вместо речи 15 минут");
        conditionList.add("Твой разум помутился, неси бред в течение 15 минут");
        conditionList.add("Твой разум помутился, неси бред на тему мирового заговора в течение 15 минут");
        conditionList.add("Похоть одолела тебя, приставай к противоположному полу в течение 15 минут");
        conditionList.add("Ты почувствовал любовь к ближним. Все люди твои братья в течение 30 минут");
        conditionList.add("Ты почувствовал ненависть к ближним. Все люди твои враги в течение 30 минут");
        conditionList.add("Пойло резко ударило тебе в голову. Ты ОЧЕНЬ пьян в течение 30 минут");
        conditionList.add("Ты очень любишь деньги. Клянчи деньги у окружающих 30 минут");
        conditionList.add("Ты очень любишь давать деньги. Раздавай деньги окружающим 30 минут");
        conditionList.add("Ты очень любишь квас. Ищи квас в зоне в течение 30 минут, все задания подождут");
        conditionList.add("Ты очень любишь мороженное. Ищи мороженное в зоне в течение 30 минут, все задания подождут");
        conditionList.add("Ты очень любишь семочки. Ищи семочки в зоне в течение 30 минут, все задания подождут");
        conditionList.add("Ты очень любишь водку. Ищи водку в зоне в течение 30 минут, все задания подождут");
        conditionList.add("Ты очень любишь паштет. Ищи паштет в зоне в течение 30 минут, все задания подождут");
        conditionList.add("Пойло резко ударило тебе в голову. Ты почти не стоишь на ногах в течение 30 минут");
        conditionList.add("Ты чувствуешь острое желание выговориться. Рассказывай всем о своих проблемах 30 минут");
        conditionList.add("Ты чувствуешь острое желание выслушать. Расспрашивай всех о их проблемах 30 минут");
        conditionList.add("Ты хочешь сделать отверстие. Одержим идеей сверлить в течение 30 минут");
        conditionList.add("Ты хочешь забить гвоздь. Одержим желанием забить гвоздь в течение 30 минут");
        conditionList.add("Ты видишь вокруг себя животных вместо людей 30 минут");
        conditionList.add("Ты видишь вокруг себя предметы мебели вместо людей 30 минут");
        conditionList.add("Ты видишь вокруг себя монстров вместо людей 30 минут");
        conditionList.add("Ты поэт. Изьясняйся стихами и интонациями Маяковского в течение 15 минут");
        conditionList.add("Ты поэт. Изьясняйся стихами и интонациями Пушкина в течение 15 минут");
        conditionList.add("Ты вспомнил корни. Изьясняйся на украинском языке в течение 30 минут");
        conditionList.add("Ты иностранный шпион. Изьясняйся на английском языке как умеешь в течение 15 минут");
        conditionList.add("Ты иностранный шпион. Изьясняйся на китайском языке как умеешь в течение 10 минут");
        conditionList.add("Тебе кажется что все стали очень большими, удивляйся в течение 20 минут");
        conditionList.add("Тебе кажется что все стали очень маленькими, удивляйся в течение 20 минут");
        conditionList.add("Тебе кажется что все хотят тебя убить, убегай от людей в течение 20 минут");

        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int randomNum = ThreadLocalRandom.current().nextInt(0, conditionList.size()-1);
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        textView.setText("Сканирование...");
                        return true;
                    case MotionEvent.ACTION_UP:
                        textView.setText(conditionList.get(randomNum));
                        return true;
                }
                return false;
            }
        });
    }

    public void setNullValue(View v){
        textView.setText("Готов к сканированию");
    }
}
